<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Carson Schultz">
	<link rel="icon" href="~/../img/favicon.ico">

	<title>My Online Portfolio - Carson Schultz</title>

	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	text-shadow: 5px 5px 50px rgb(0, 0, 0);
	color: rgb(255, 255, 255);
	padding-top: 35px;
	font-size: 40px;
	font-family: "verdana", sans-serif;    
}
.item
{
	background: #666;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>
</head>

<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="1000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

			 <div class="active item" style="background: url(~/../img/slide1.jpg) no-repeat left center; background-size: cover;">
				 <div class="container">
					 <div class="carousel-caption">
						<h2>My Bitbucket Course Repository</h2>
						<h3>Advanced Web Development</h3>
						<a class="btn btn-large btn-primary" href="https://bitbucket.org/cls18c/lis4368/src/master/">Click Here</a>
					 </div>
				 </div>	
			</div>					

         	<div class="item" style="background: url(~/../img/slide2.jpg) no-repeat left center; background-size: cover;">
                <div class="container">
               		<div class="carousel-caption">
						<h2>My Website</h2>
                		<h3>carsonleighschultz.com</h3>
						<a class="btn btn-large btn-primary" href="https://carsonleighschultz.com/">Click Here</a>
					</div>
						<!--  <img src="img/slide2.png" alt="Slide 2">									 -->						
                </div>
            </div>

         	<div class="item" style="background: url(~/../img/slide3.jpg) no-repeat left center; background-size: cover;">
				<div class="container">
					<div class="carousel-caption">
						<h2>My LinkedIn</h2>
						<h3>Let's Connect</h3>
						<a class="btn btn-large btn-primary" href="https://www.linkedin.com/in/carson-leigh-schultz">Click Here</a>
				 	</div>
					 <!--  <img src="img/slide2.png" alt="Slide 2">									 -->						
			 	</div>
            </div>

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>
