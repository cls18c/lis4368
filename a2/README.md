> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Carson Schultz

### Assignment 2 Requirements:

*Three Parts:*

1. MySQL Workbench Installation and Creation of Local Connection
2. Servlet Use and Compilation
3. Chapter Questions (Chs. 5, 6)

#### README.md file should include the following items:

* Screenshot of Running [http://localhost:9999/hello](http://localhost:9999/hello)
* Screenshot of Running [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html) 
* Screenshot of Running [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
* Screenshot of Running [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
* Bitbucket repo link to this assignment
* Screenshots of Skill Sets 1 - 3

<br />

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello:*

![http://localhost:9999/hello Screenshot](~/../../img/hello.png)

*Screenshot of http://localhost:9999/hello/HelloHome.html:*

![http://localhost:9999/hello/HelloHome.html Screenshot](~/../../img/helloIndex.png)

*Screenshot of http://localhost:9999/hello/sayhello:*

![http://localhost:9999/hello/sayhello Screenshot](~/../../img/sayhello.png)

*Screenshot of http://localhost:9999/hello/querybook.html:*

![http://localhost:9999/hello/querybook.html Screenshot](~/../../img/querybook.png)

*Screenshot of hhttp://localhost:9999/hello/query?author=Tan+Ah+Teck:*

![http://localhost:9999/hello/query?author=Tan+Ah+Teck Screenshot](~/../../img/query.png)

*Screenshot of a2/index.jsp:*

![a2/index.jsp Screenshot](~/../../img/a2index.png)

<br />

#### Links:

*Bitbucket - Assignment 2:*
[A2 Bitbucket Link](https://bitbucket.org/cls18c/lis4368/src/master/a2/ "Bitbucket Assignment 2")

*Java Servlet Tutorial:*
[Java Servlet Tutorial](https://personal.ntu.edu.sg/ehchua/programming/howto/Tomcat_HowTo.html "Java Servlet Tutorial")

<br />

#### Skill Sets 1 - 3:

*Screenshot of Q1_System_Properties:*

![Q1_System_Properties Screenshot](~/../../img/Q1_System_Properties.png)

*Screenshot of Q2_Looping_Structures:*

![Q2_Looping_Structures Screenshot](~/../../img/Q2_Looping_Structures.png)

*Screenshot of Q3_Number_Swap:*

![Q3_Number_Swap Screenshot](~/../../img/Q3_Number_Swap.png)
