> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Carson Schultz

### Project 1 Requirements:

*Four Parts:*

1. Add jQuery Validation as per Entity Attribute Requirements
2. Add jQuery Regular Expressions as per Entity Attribute Requirements
3. Bitbucket Repo Link to This Project
4. Chapter Questions (Chs. 9, 10)

#### README.md file should include the following items:

* Screenshots of Data Validation Form (p1/index.jsp)
* Screenshot of Successful Data Validation 
* Screenshot of Failed Data Validation 
* Skill sets 7 - 9 

<br />

#### Project Screenshots:

*Screenshot of p1/index.jsp*:
![p1/index.jsp Screenshot](~/../../img/p1index.png)

*Screenshot of Successful Validation*:
![Successful Validation Screenshot](~/../../img/p1_Successful_Validation.png)

*Screenshot of Failed Validation*:
![Failed Validation Screenshot](~/../../img/p1_Failed_Validation.png)

<br />

#### Skill Sets 7 - 9

*Screenshot of Q7_Count_Characters*:
![Q7_Count_Characters Screenshot](~/../../img/Q7_Count_Characters.png)

<br />

*Screenshot of Q8_ASCII*:
![Q8_ASCII_Part1 Screenshot](~/../../img/Q8_ASCII_Part1.png)
![Q8_ASCII_Part2 Screenshot](~/../../img/Q8_ASCII_Part2.png)
![Q8_ASCII_Part3 Screenshot](~/../../img/Q8_ASCII_Part3.png)
![Q8_ASCII_Part4 Screenshot](~/../../img/Q8_ASCII_Part4.png)

<br />

*Screenshot of Q9_Grade_Calculator*:
![Q9_Grade_Calculator Screenshot](~/../../img/Q9_Grade_Calculator.png)

<br />

#### Project Link:

*Bitbucket Project 1 Link:*
[P1 Bitbucket Link](https://bitbucket.org/cls18c/lis4368/src/master/p1/ "P1 Bitbucket Link")
