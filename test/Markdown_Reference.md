<!-- Headings -->
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

<!-- Italics -->
*This text is italic*

<!-- Bold -->
**This text is bolded**

<!-- Strikethrough -->
~~This text is strikethrough~~

<!-- Horrizontal Rule -->
Here's a line:
___

<!-- Blockquote -->
> This is a blockquote

<!-- Links -->
[Embedded Link](http://www.link.com "Displayed When Hovered")
