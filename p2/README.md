> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Carson Schultz

### Project 2 Requirements:

*Two Parts:*

1. Complete JSP/Servlets web application using the MVC framework
2. Providing create, read, update, and delete functionality

<br />

#### README.md file should include the following items:

* Screenshot of Pre-valid User Form Entry
* Screenshot of Post-Valid User Form Entry
* Screenshot of MySQL Select, Insert, Update, and Delete

<br />

#### Project Screenshots:

*Screenshot of Pre-Valid Customer Form Entry*:

![Pre-Valid User Form Entry Screenshot](~/../../img/PreValid.png)

*Screenshot of Post-Valid Customer Form Entry*:

![Post-Valid User Form Entry Screenshot](~/../../img/PostValid.png)

*Screenshot of Customer Table Added Entry*:

![Customer Table Entry Add Screenshot](~/../../img/Add.png)

*Screenshot of Customer Form Update*:

![Customer Form Update Screenshot](~/../../img/FormUpdate.png)

*Screenshot of Customer Table Entry Update*:

![Customer Table Entry Update Screenshot](~/../../img/Update.png)

*Screenshot of Customer Table Delete Message*:

![Customer Table Delete Message Screenshot](~/../../img/DeleteMessage.png)

*Screenshot of Associated Database Changes (Select, Insert, Update, Delete)*:

![Associated Database Changes (Select, Insert, Update, Delete) Screenshot](~/../../img/Customer_MySQL.png)

<br />

#### Project Link:

*Bitbucket Project 2 Link:*
[P2 Bitbucket Link](https://bitbucket.org/cls18c/lis4368/src/master/p2/ "P2 Bitbucket Link")

