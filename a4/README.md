> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Carson Schultz

### Assignment 4 Requirements:

*Four Parts:*

1. Add jQuery Validation as per Entity Attribute Requirements
2. Add jQuery Regular Expressions as per Entity Attribute Requirements
3. Bitbucket Repo Link to This Project
4. Chapter Questions (Chs. 11, 12)

#### README.md file should include the following items:

* Screenshots of Data Validation Form
* Screenshot of Successful Data Validation 
* Screenshot of Failed Data Validation 
* Skill Sets 10 - 12

<br />

#### Assignment Screenshots:

*Screenshot of Successful Validation*:
![Successful Validation Screenshot](~/../../img/a4_Successful_Validation.png)

*Screenshot of Failed Validation*:
![Failed Validation Screenshot](~/../../img/a4_Failed_Validation.png)

*Record Successfully Added*:
![Successful Record Screenshot](~/../../img/a4_Adding_Record.png)

<br />

#### Skill Sets 10 - 12

*Screenshot of Q10_Simple_Calculator:*

![Q10_Simple_Calculator Run 1 Screenshot](~/../../img/Q10_Simple_Calculator_Run1.png)
![Q10_Simple_Calculator Run 2 Screenshot](~/../../img/Q10_Simple_Calculator_Run2.png)
![Q10_Simple_Calculator Run 3 Screenshot](~/../../img/Q10_Simple_Calculator_Run3.png)
![Q10_Simple_Calculator Run 4 Screenshot](~/../../img/Q10_Simple_Calculator_Run4.png)
![Q10_Simple_Calculator Run 5 Screenshot](~/../../img/Q10_Simple_Calculator_Run5.png)
![Q10_Simple_Calculator Run 6 Screenshot](~/../../img/Q10_Simple_Calculator_Run6.png)

<br />

*Screenshot of Q11_Compound_Interest_Calculator:*

![Q11_Compound_Interest_Calculator Screenshot](~/../../img/Q11_Compound_Interest_Calculator.png)

<br />

*Screenshot of Q12_Copy_Array:*

![Q12_Copy_Array Screenshot](~/../../img/Q12_Copy_Array.png)

<br />

#### Assignment Link:

*Bitbucket - Assignment 4:*
[A4 Bitbucket Link](https://bitbucket.org/cls18c/lis4368/src/master/a4/ "Bitbucket Assignment 4")



