> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Carson Schultz

### Assignment 5 Requirements:

*Requirements:*

1. Prepared statements to help prevent SQL injection.
2. JSTL to prevent XSS
3. Adds insert functionality to A4

<br />

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Associated mySQL Entry
* Bitbucket Link to this Assignment
* Skill Sets 13 - 15

<br />

#### Assignment Screenshots:

*Screenshot of Valid User Form Entry*:

![Valid User Form Entry](~/../../img/DataEntry.png)

<br />

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](~/../../img/RecordEntrySuccess.png)

<br />

*Screenshot of Associated Entry - mySQL*:

![mySQL Associated Entry Screenshot](~/../../img/mySQL.png)

<br />

#### Skill Sets 13 - 15:

*Screenshot of Q13_File_Read_Write_Count_Words*:

![Q13_File_Read_Write_Count_Words](~/../../img/Q13_File_Write_Read_Count_Words.png)

<br />

*Screenshot of Q14_Vehicle*:

![Q14_Vehicle](~/../../img/Q14_Vehicle.png)

<br />

*Screenshot of Q15_Vehicle_Inheritence*:

![Q15_Vehicle_Inheritence](~/../../img/Q15_Vehicle_Inheritence.png)

<br />

#### Assignment Links:

*Bitbucket Assignment 5:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


