-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema cls18c
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `cls18c` ;

-- -----------------------------------------------------
-- Schema cls18c
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cls18c` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `cls18c` ;

-- -----------------------------------------------------
-- Table `cls18c`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cls18c`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cls18c`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` CHAR(9) NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cls18c`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cls18c`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cls18c`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` CHAR(9) NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cls18c`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cls18c`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cls18c`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `cls18c`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `cls18c`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `cls18c`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `cls18c`;
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'Pets - R - Us', 'Mohave Way', 'Jacksonville', 'FL', '32259', 9044607899, 'petsrus@example.com', 'http://www.petsrus.com', 10650.45, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', 'Jefferson Street', 'Tallahassee', 'FL', '32304', 8507893412, 'furryfriends@example.com', 'http://www.furryfriends.com', 23450.00, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Corner Store', 'Jackson Street', 'Tallahassee', 'FL', '32304', 8506721345, 'petcornerstore@example.com', 'http://www.petcornerstore.com', 108890.80, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Woof Gang', 'Monroe Way', 'Jacksonville', 'FL', '32258', 9045612478, 'woofgang@example.com', 'http://www.woofgang.com', 97008.50, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Shop', 'Copeland Street', 'Tallahassee', 'FL', '32305', 8506782367, 'petshop@example.com', 'http://www.petshop.com', 40500.95, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Happy Paws', 'NW 13th', 'St. Johns', 'FL', '32259', 9045987688, 'happypaws@example.com', 'http://www.happypaws.com', 35450.00, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Exotic Pets', 'East Monroe Street', 'Tallahassee', 'FL', '32305', 8509872345, 'exoticpets@example.com', 'http://www.exoticpets.com', 65000.00, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Land', 'Blackhawk Way', 'Jacksonville', 'FL', '32304', 8509002000, 'petland@example.com', 'http://www.petland.com', 100500.60, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Care Center', 'Easy Street', 'St. Johns', 'FL', '32259', 9044589001, 'petcarecenter@example.com', 'http://www.petcarecenter.com', 85600.96, NULL);
INSERT INTO `cls18c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pets Unlimited', 'Magnolia Street', 'Jacksonville', 'FL', '32258', 9042110098, 'petsunlimited@example.com', 'http://www.petsunlimited.com', 80500.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cls18c`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `cls18c`;
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'Mary', 'Trawick', 'Blackhawk Street', 'Tallahassee', 'FL', '32245', 8508931245, 'marytrawick@example.com', 100.00, 150.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Justin', 'Martinez', 'Mohave Way', 'Jacksonville', 'FL', '32259', 9044571234, 'justinmartinez@example.com', 500.00, 1000.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Carson', 'Hafner', 'Copeland Street', 'Tallahassee', 'FL', '32304', 8509045678, 'carsonhafner@example.com', 600.00, 800.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Eric', 'Carpenter', 'Jefferson Street', 'Jacksonville', 'FL', '32304', 9041123122, 'ericcarpenter@example.com', 54.00, 65.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jason', 'Waterman', 'Monroe Street', 'Jacksonville', 'FL', '32305', 9044609877, 'jasonwaterman@example.com', 10.50, 100.90, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Will', 'Schultz', 'East Monre Street', 'Tallahassee', 'FL', '32259', 8503124334, 'willschultz@example.com', 1000.50, 2050.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Lauren', 'Andrews', 'North Jefferson Street', 'Tallahassee', 'FL', '32258', 8509081123, 'laurenandrews@example.com', 400.00, 500.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Grace', 'Williams', 'ABC Street', 'Tallahassee', 'FL', '32259', 8501124511, 'gracewilliams@example.com', 450.00, 500.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Ann', 'Jacobs', 'NE 7th', 'Jacksonville', 'FL', '32304', 9044314556, 'annjacobs@example.com', 900.00, 1000.00, NULL);
INSERT INTO `cls18c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Taylor', 'Colling', 'Green Circle Street', 'Jacksonville', 'FL', '32304', 9048769112, 'taylorcolling@example.com', 95.00, 100.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cls18c`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `cls18c`;
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 2, 1, 'German Shepard', 'f', 5000.00, 5500.00, 6, 'brown', '2022-01-21', 'y', 'y', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 4, 2, 'Golden Retriever', 'm', 6000.00, 6500.00, 5, 'yellow', '2021-09-13', 'y', 'y', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 6, 5, 'Parrot', 'f', 600.00, 650.00, 4, 'green', '2021-12-25', 'y', 'y', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 6, 7, 'Peacock', 'm', 750.00, 800.00, 3, 'green/blue', '2021-01-01', 'y', 'n', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 1, 10, 'Gooden Doodle', 'f', 8000.00, 8500.00, 7, 'yellow', '2022-01-02', 'n', 'n', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 3, 10, 'Poodle', 'm', 7500.00, 8000.00, 4, 'white', '2020-08-17', 'y', 'n', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 5, 8, 'Weasel', 'f', 75.00, 100.00, 6, 'brown', '2019-07-16', 'n', 'y', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 5, 3, 'Bulldog', 'm', 6575.00, 7000.00, 2, 'gray', '2019-11-30', 'n', 'y', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 6, 1, 'Guinea Pig', 'f', 700.00, 800.00, 8, 'brown', '2020-12-18', 'y', 'y', NULL);
INSERT INTO `cls18c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 1, 7, 'Labrador', 'm', 6400.00, 6800.00, 12, 'black', '2022-02-08', 'y', 'y', NULL);

COMMIT;

