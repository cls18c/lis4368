> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Carson Schultz

### Assignment 3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include Data (at least 10 records each table)
3. Provide Bitbucket Read-Only Access to Repo and Links to the Following Files:
    - docs folder: a3.mwb, and a3.sql
    - img folder: a3.png (export a3.mwb)
4. Bitbucket Repo Link to This Assignment
5. Chapter Questions (Chs. 7, 8)

#### README.md file should include the following items:

* Screenshot of ERD that links to the image
* Screenshots of Skill Sets 4 - 6

<br />

#### Assignment Screenshots:

*Screenshot of A3 ERD*: 
 
![A3 ERD](~/../../img/a3.png "ERD based upon A3 Requirements") 


*Screenshot of A3 ERD Table Records*:

![A3 ERD](~/../../img/a3_Petstore_Records.png "10 Petstore Records")

![A3 ERD](~/../../img/a3_Pet_Records.png "10 Pet Records") 

![A3 ERD](~/../../img/a3_Customer_Records.png "10 Customer Records") 

<br />
 
*A3 docs: a3.mwb and a3.sql*: 
 
[A3 MWB File](~/../docs/a3.mwb "A3 ERD in .mwb format") 
 
[A3 SQL File](~/../docs/a3.sql "A3 SQL Script") 

<br />

#### SQL Statements 

1. List only the pet store IDs, full address, and phone number for all of the pet stores. 
> select pst_id, pst_street, pst_city, pst_state, pst_zip, pst_phone <br />
> from petstore; 

2. Display the pet store name, along with the number of pets each pet store has. 
> select pst_name, count(pet_id) as 'number of pets' <br />
> from petstore <br />
> natural join pet <br />
> group by pst_id; <br />

3. List each pet store ID, along with all of the customer first, last names and balances 
   associated with each pet store. 
> select pst_id, cus_fname, cus_lname, cus_balance <br />
> from petstore <br />
> natural join pet <br />
> natural join customer; <br />

4. Update the customer last name to 'Valens' for Customer #2. 
> update customer <br />
> set cus_lname = 'Valens' <br />
> where cus_id = 2; <br />
> // confirming changes <br />
> select cus_fname, cus_lname <br />
> from customer; <br />

5. Remove Pet #4. 
> delete from pet <br />
> where pet_id = 4;

6. Add two more customers. 
> insert into customer (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) <br />
> value (DEFAULT, 'Jacob', 'Lawson', 'Green Myrtle Drive', 'Jacksonville','FL', '32258', 9048769789, 'jacoblawson@example.com', 195.00, 200.00, NULL), <br />
>	    (DEFAULT, 'Natalia', 'Nelson', 'Doak Road', 'Tallahassee','FL', '32305', 8509067122, 'natalianelson@example.com', 325.00, 500.00, NULL);

<br />

#### Skill Sets 4 - 6

*Screenshot of Q4_Directory_Info:*

![Q4_Directory_Info Screenshot](~/../../img/Q4_Directory_Info.png)

<br />

*Screenshot of Q5_Character_Info:*

![Q5_Character_Info_Run1 Screenshot](~/../../img/Q5_Character_Info_Run1.png)
![Q5_Character_Info_Run2 Screenshot](~/../../img/Q5_Character_Info_Run2.png)
![Q5_Character_Info_Run3 Screenshot](~/../../img/Q5_Character_Info_Run3.png)

<br />

*Screenshot of Q6_Determine_Character:*

![Q6_Determine_Character_Run1 Screenshot](~/../../img/Q6_Determine_Character_Run1.png)
![Q6_Determine_Character_Run2 Screenshot](~/../../img/Q6_Determine_Character_Run2.png)
![Q6_Determine_Character_Run3 Screenshot](~/../../img/Q6_Determine_Character_Run3.png)
![Q6_Determine_Character_Run4 Screenshot](~/../../img/Q6_Determine_Character_Run4.png)
![Q6_Determine_Character_Run5 Screenshot](~/../../img/Q6_Determine_Character_Run5.png)

<br />

*Screenshot of a3/index.jsp:*

![a3/index.jsp Screenshot](~/../../img/a3index.png)

#### Assignment Link:

*Bitbucket - Assignment 3:*
[A3 Bitbucket Link](https://bitbucket.org/cls18c/lis4368/src/master/a3/ "Bitbucket Assignment 3")

