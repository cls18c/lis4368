> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Carson Schultz

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs. 1 - 4)

#### README.md file should include the following items:

* Screenshot of Running java Hello (#1 above);
* Screenshot of Running http://localhost:9999 (#2 above, #4(b) in tutorial);
* Git Commands with Short Descriptions;
* Bitbucket Repo Links: a) This Assignment and b) The Completed Tutorial Above (bitbucketstationlocations)

<br />

#### Git commands w/short descriptions:

1. git init - create a new local repository
2. git status - list the files you've changed and those you still need to add or commit
3. git add - add files to staging
4. git commit - move files from staging to a commit; tracks any changes
5. git push - send changes to the master branch of your remote repository
6. git pull - fetch and merge changes on the remote server to your working directory
7. git remote - connect to a remote repository

<br />

#### Assignment Screenshots:

*Screenshot of running java Hello:*

![Running java Hello Screenshot](~/../../img/javaHello.png)

*Screenshot of running http://localhost:9999:*

![JDK Installation Screenshot](~/../../img/running9999.png)

*Screenshot of a1/index.jsp:*

![a1/index.jsp Screenshot](~/../../img/a1index.png)

<br />

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/cls18c/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Bitbucket - Assignment 1:*
[A1 Bitbucket Link](https://bitbucket.org/cls18c/lis4368/src/master/a1/ "Bitbucket Assignment 1")

