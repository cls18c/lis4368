> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368

## Carson Schultz

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide Screenshots of Installations
    - Create Bitbucket repo
    - Complete Bitbucket Tutorial (bitbucketstationlocations)
    - Provide Git Commands and Descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL Workbench
    - Create Local Connection
    - Create ebookshop Database
    - Compile Servlet Files
    - Provide Screenshots of Assignment Links
    - Skill Sets 1 - 3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Entity Relationship Diagram (ERD)
    - Insert Records and Provide Screenshots
    - Provide Links to a3.mwb and a3.sql
    - Skill Sets 4 - 6

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Client-Side Validation
    - Create Form Controls to Match Entity Attributes
    - Add jQuery Validations 
    - Add jQuery Regular Expressions
    - Provide Form Validation Screenshots
    - Skill Sets 7 - 9

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server-Side Validation
    - Create Form Controls to Match Entity Attributes
    - Add jQuery Validations
    - Add jQuery Regular Expressions
    - Provide Form Validation Screenshots
    - Skill Sets 10 - 12

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Adding Insert Functionality to A4
    - Provide Form Validation Screenshots
    - Skill Sets 13 - 15

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Server-Side Validation
    - Client-Side Validation
    - Prepared Statements to Help Prevent SQL Injection 
    - JSTL to Prevent XSS
    - Completes CRUD Functionality