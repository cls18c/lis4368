package Q10_Simple_Calculator;
import java.util.Scanner;

public class Methods 
{
    public static void getRequirements()
    {
        System.out.println("");
        System.out.println("Developer: Carson Schultz");
        System.out.println("Program Requirements:");
        System.out.println("\t1. Program uses methods to: add, subtract, multiply, divide, ");
        System.out.println("\t2. Program checks for non-numeric and out of range values.");
        System.out.println("Note: Program is accurate to 6th fraction digit (i.e., 6 digits right of decimal point).");
        System.out.println("");
    } // end of getRequirements

    public static void calculateNumbers()
    {
        double num1 = 0.0, num2 = 0.0;
        char operation = ' ';

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter mathematical operation (a = addition, s = subtraction, m = multiplication, d = division, e = exponentiation): ");

        operation = sc.next().toLowerCase().charAt(0);

        while (operation != 'a' && operation != 's' && operation != 'm' && operation != 'd' && operation != 'e')
        {
            System.out.print("\nIncorrect operation. Please enter correct operation: ");
            operation = sc.next().toLowerCase().charAt(0);
        }

        System.out.print("Please enter first number: ");
        while(!sc.hasNextDouble())
        {
            System.out.print("Not a valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter first number: ");
        }
        num1 = sc.nextDouble();

        System.out.print("Please enter second number: ");
        while(!sc.hasNextDouble())
        {
            System.out.print("Not a valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = sc.nextDouble();

        if(operation == 'a')
        {
            Addition(num1, num2);
        } // if addition is selected

        else if(operation == 's')
        {
            Subtraction(num1, num2);
        } // if subtraction is selected

        else if(operation == 'm')
        {
            Multiplication(num1, num2);
        } // if multiplication is selected

        else if(operation == 'd')
        {
            if (num2 == 0)
            {
                System.out.print("Cannot divide by zero!");
            }
            else
            {
                Division(num1, num2);
            }
        } // if division is selected

        else if(operation == 'e')
        {
            Exponentiation(num1, num2);
        }

        System.out.println();

        sc.close();

    } // end of calculateNumbers

    public static void Addition(double n1, double n2)
    {
        System.out.print(n1 + " + " + n2 + " = ");
        System.out.printf("%.2f", (n1 + n2));
        System.out.println();
    }  // end of Addition 

    public static void Subtraction(double n1, double n2)
    {
        System.out.print(n1 + " - " + n2 + " = ");
        System.out.printf("%.2f", (n1 - n2));
        System.out.println();
    } // end of Subtraction

    public static void Multiplication(double n1, double n2)
    {
        System.out.print(n1 + " * " + n2 + " = ");
        System.out.printf("%.2f", (n1 * n2));
        System.out.println();
    } // end of Multiplication

    public static void Division(double n1, double n2)
    {
        System.out.print(n1 + " / " + n2 + " = ");
        System.out.printf("%.2f", (n1 / n2));
        System.out.println();
    } // end of Division

    public static void Exponentiation(double n1, double n2)
    {
        System.out.print(n1 + " to the power of " + n2 + " = ");
        System.out.printf("%.2f", (Math.pow(n1, n2)));
        System.out.println();
    } // end of Exponentiation


} // end of methods