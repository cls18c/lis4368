package Q4_Directory_Info; 
import java.io.File;
import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("");
        System.out.println("Developer: Carson Schultz");
        System.out.println("Program lists files and subdirectories or user-specified directory.");
        System.out.println("");
    } // end of getRequirements()

    public static void directoryInfo()
    {
        // initializing variables
        String myDir = "";
        Scanner sc = new Scanner(System.in);

        // user entered directory path
        System.out.print("Please enter directory path: ");
        myDir = sc.nextLine();

        // create File object
        File directoryPath = new File(myDir);

        try
        {
            File[] files = directoryPath.listFiles();

            // display file information
            for (int i = 0; i < files.length; i++)
            {
                System.out.println("Name: " + files[i].getName());                    System.out.println("Path: " + files[i].getAbsolutePath());
                System.out.println("Size (Bytes): " + files[i].length());
                System.out.println("Size (KB): " + files[i].length()/(1024));
                System.out.println("Size (MB): " + files[i].length()/(1024*1024));
                System.out.println("");
            }            
        }   

        catch (Exception e) 
        {
            // catch errors and print message
            System.err.println(e.getMessage());
        }

    sc.close();

    } // end of directoryInfo()
} // end of Methods