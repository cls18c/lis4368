package Q9_Grade_Calculator;
import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("");
        System.out.println("Developer: Carson Schultz");
        System.out.println("Program Requirements:\n\t1. Returns letter grade, based upon user-entered numeric score.");
        System.out.println("\t2. Program checks for non-numeric and out of range values.");
        System.out.println("Note: Program is accurate to 6th fraction digit (i.e., 6 digits right of decimal point).");
        System.out.println("");
    }

    public static void getScore()
    {
        // initialize variables 
        Scanner sc = new Scanner(System.in);
        double score = 0.0;

        // prompt user for entry
        System.out.print("Please enter a grade between 0 - 100, inclusive: ");

        // while user input is NOT a double ...
        while (!sc.hasNextDouble())
        {
            System.out.println("Not a valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter grade between 0 - 100, inclusive: ");
        } // loops until user enters double 

        // storing double in score variable
        score = sc.nextDouble();
        // using score as parameter for getGrade() method
        getGrade(score);

        sc.close();
    }

    public static void getGrade(double score)
    {
        String grade = "";

        if (score < 0 || score > 100)
        {
            // tests for out of range scores
            System.out.println("\nOut of range!");
            //sc.next();
            getScore();
        }
        else 
        {
            // score is between 0 - 59
            if (score < 60 && score >= 0)
            {
                grade = "F";
            }
            
            // score is between 60 - 69
            else if (score < 63 && score >= 60)
            {
                grade = "D-";
            }
            else if (score < 67 && score >= 63)
            {
                grade = "D";
            }
            else if (score < 70 && score >= 67)
            {
                grade = "D+";
            }

            // score is between 70 - 79
            else if (score < 73 && score >= 70)
            {
                grade = "C-";
            }
            else if (score < 77 && score >= 73)
            {
                grade = "C";
            }
            else if (score < 80 && score >= 77)
            {
                grade = "C+";
            }

            // score is between 80 - 89
            else if (score < 83 && score >= 80)
            {
                grade = "B-";
            }
            else if (score < 87 && score >= 83)
            {
                grade = "B";
            }
            else if (score < 90 && score >= 87)
            {
                grade = "B+";
            }

            // score is between 90 - 92
            else if (score < 93 && score >= 90)
            {
                grade = "A-";
            }

            // score is 93 - 100
            else 
            {
                grade = "A";
            }

            System.out.printf("\nScore Entered: %f%n", score);
            System.out.printf("Final Grade: %s%n", grade);
            System.out.println("");

        }


    }
}