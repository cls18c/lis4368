package Q12_Copy_Array;

class Main 
{
    public static void main(String args[])
    {
        Methods.getRequirements();
        String[] userArray = Methods.createArray();
        Methods.copyArray(userArray);
    }
}
