package Q15_Vehicle_Inheritence;


class Car extends Vehicle 
{
    private float speed;

    public Car()
    {
        super();   
        speed = 100;
        System.out.println("\nInside car default constructor.");
    }

    public Car(String make, String model, int year, float speed)
    {
        super(make, model, year);
        this.speed = speed;
        System.out.println("\nInside car constructor with parameters.");
    }

    public float getSpeed()
    {
        return speed;
    }

    public void setSpeed(float s)
    {
        speed = s;
    }

    public void print()
    {
        super.print();
        System.out.println(", Speed: " + speed);
    }
}