package Q15_Vehicle_Inheritence;
import java.util.Scanner;

//import org.eclipse.jdt.internal.compiler.ast.SuperReference;

class VehicleDemo
{
    public static void main(String[] args)
    {
        String mk = "";
        String md = "";
        int yr = 0;
        float s = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("\nBelow are base class default constructor values: ");

        Vehicle v1 = new Vehicle();
        System.out.println("\nMake = " + v1.getMake());
        System.out.println("Model = " + v1.getModel());
        System.out.println("Year = " + v1.getYear());

        System.out.println("\nBelow are base class user-entered values: ");

        System.out.print("\nMake: ");
        mk = sc.nextLine();
        System.out.print("Model: ");
        md = sc.nextLine();
        System.out.print("Year: ");
        yr = sc.nextInt();

        Vehicle v2 = new Vehicle(mk, md, yr);
        System.out.println("\nMake = " + v2.getMake());
        System.out.println("Model = " + v2.getModel());
        System.out.println("Year = " + v2.getYear());

        System.out.println("\nBelow using setter methods to pass literal vlues, then print() method to display values: ");
        
        System.out.println("");
        v2.setMake("Chevrolet");
        v2.setModel("Corvette Z06");
        v2.setYear(2022);
        v2.print();
        System.out.println("");


        System.out.println("\n\nBelow are dervied class default constructor values: ");

        Car c1 = new Car();
        System.out.println("\nMake = " + c1.getMake());
        System.out.println("Model = " + c1.getModel());
        System.out.println("Year = " + c1.getYear());
        System.out.println("Speed = " + c1.getSpeed());

        System.out.println("\nBelow are derived class user-entered values: ");

        System.out.print("\nMake: ");
        mk = sc.next();
        System.out.print("Model: ");
        md = sc.next();
        System.out.print("Year: ");
        yr = sc.nextInt();
        System.out.print("Speed: ");
        s = sc.nextFloat();

        Car c2 = new Car(mk, md, yr, s);
        System.out.println("\nMake = " + c2.getMake());
        System.out.println("Model = " + c2.getModel());
        System.out.println("Year = " + c2.getYear());
        System.out.println("Speed = " + c2.getSpeed());

        System.out.println("\nBelow using setter methods to pass literal vlues, then print() method to display values: ");
        
        System.out.println("");
        c2.setMake("Chevrolet");
        c2.setModel("Corvette Z06");
        c2.setYear(2022);
        c2.setSpeed(180);
        c2.print();
        System.out.println("");
        
    }


}
