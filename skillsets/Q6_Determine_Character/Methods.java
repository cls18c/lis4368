package Q6_Determine_Character;

import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("");
        System.out.println("Developer: Carson Schultz");
        System.out.println("Program determines whether user-entered value is vowel, consonant,\nspecial character, or integer.");
        System.out.println("Program displays character's ASCII value.");
        System.out.println("");
        
        System.out.println("References: ");
        System.out.println("ASCII Background: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://www.ascii-code.com/");
        System.out.println("Lookup Tables: https://www.lookuptables.com\n");
    }

    public static void determineChar()
    {
        char ch = ' ';
        char chTest = ' ';
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter a character: ");
        ch = sc.next().charAt(0);
        chTest = Character.toLowerCase(ch);

        if (chTest == 'a' || chTest == 'e' || chTest == 'i' || chTest == 'o' || chTest == 'u' || chTest == 'y')
        {
            System.out.println(ch + " is vowel. ASCII value: " + (int) ch);
            System.out.println("");
        }

        else if (ch >= '0' && ch <= '9')
        {
            System.out.println(ch + " is integer. ASCII value: " + (int)ch);
            System.out.println("");
        }

        else if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
            System.out.println(ch + " is a consonant. ASCII value: " + (int) ch);
            System.out.println("");
        }

        else 
        {
            System.out.println(ch + " is a special character. ASCII values: " + (int)ch);
            System.out.println("");
        }

        sc.close();
    }
}