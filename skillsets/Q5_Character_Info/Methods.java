package Q5_Character_Info;

import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("");
        System.out.println("Developer: Carson Schultz");
        System.out.println("Program determines a total number of characters in a line of text,\nas well as number of times a specific character is used.");
        System.out.println("Program displays character's ASCII values.");
        System.out.println("");

        System.out.println("References: ");
        System.out.println("ASCII Background: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://www.ascii-code.com/");
        System.out.println("Lookup Tables: https://www.lookuptables.com\n");
    }

    public static void getCharInfo()
    {
        int num = 0;
        String userInput = "";
        Scanner sc = new Scanner(System.in);

        // user entered directory path
        System.out.print("Please enter a line of text: ");
        userInput = sc.nextLine();
            
        int totalCount = userInput.length();

        System.out.print("Please enter character to check: ");
        char ch = sc.next().charAt(0);

        for (int i = 0; i < totalCount; i++)
        {
            if (ch == userInput.charAt(i))    
            {
                ++num;
            }
        }

        System.out.println("\nNumber of characters in line of text: " + totalCount);
        System.out.println("The character " + ch + " appears " + num + " time(s) in line of text.");
        System.out.println("ASCII value: " + (int) ch);
        System.out.println("");
        
        
        sc.close();
        
    }
}
